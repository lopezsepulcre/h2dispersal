---
title: "Data"
author: "Weixin Du"
date: "10/20/2021"
output: 
  html_document:
    toc: true
    toc_float: true
    toc_depth: 4
    theme: flatly
    code_folding: hide
---

## Overview

This vignette reads the raw datafile (in csv format) and transforms it into an Rda file, to be shipped with the package.
It also extracts scaling and centering factors, to be used to back-transform variables.

## Setup
```{r, message=FALSE}
# loading required packages
library(tidyverse)
library(MCMCglmm)
library(pedigree)
library(MasterBayes)
# library(sticky)
# library(lme4)

load("../data_raw/DDDdata.rda")
pedigree <- read.csv2("../data_raw/Pedigree.csv", row.names = NULL)
# rename "FishID" column to "animal"
colnames(DDDdata)[2] <- "animal"
colnames(pedigree)[2] <- "animal"
colnames(pedigree)[6:7] <- c("sire", "dam")

# fata <- subset(data, sex == 'F')
# mata <- subset(data, sex == 'M')
```

### Clean the data
```{r}

## remove all fish in data not in pedigree (including sire & dam)
# checks if a fish in pedigree is in DDDdata, if not then it goes on the remove list
present <- unique(DDDdata$animal) %in% unique(pedigree$animal)
remove <- unique(DDDdata$animal)[which(present == FALSE)]
removable <- which(DDDdata$animal %in% remove == TRUE)
DDDdata <- as.data.frame(DDDdata[-removable, ])


# in the order required for MCMCglmm
cleanPedi <- pedigree[, c("animal", "sire", "dam")]
# convert blanks to NA, add moms and dads to the animal column
cleanPedi[cleanPedi == ""] <- NA
cleanPedi <- add.Inds(cleanPedi)


## get rid of all the duplicates
# get first instance of each fish, then keeps only those rows (assume rest are blank duplicates)
cleanPedi[cleanPedi == ""] <- NA
cleanPedi <- cleanPedi[match(unique(cleanPedi$animal), cleanPedi$animal), ]


# filter and reorder pedigree
cleanPedi <- orderPed(cleanPedi)


# present <- unique(cleanPedi$animal) %in% unique(DDDdata$animal)
# remove <- unique(cleanPedi$animal)[which(present == FALSE)]
# removable <- which(cleanPedi$animal %in% remove == TRUE)
# cleanPedi <- cleanPedi[-removable, ]
```


### FIRST TEST MODEL
- p_moving VS. s_loc_density
- in random: us(s_loc_density+1):animal + reach

```{r}
# TAKES 1.111594 days
## need to take out missing values in fixed predictors??
  # exact error message: Error in MCMCglmm(p_moving ~ s_loc_density, random = ~us(s_loc_density + : missing values in the fixed predictors

cleanData <- DDDdata[-which(is.na(DDDdata$s_loc_density)),]

prior <- list(R = list(V = diag(1), nu = 0.002),G = list(G1 = list(V = diag(2) * 0.02, nu = 4), G2 = list(V = diag(1) * 0.02, nu = 4)))

start = Sys.time()    # timing the function
model <- MCMCglmm(p_moving ~ s_loc_density, random = ~ us(s_loc_density+1):animal + reach, family = "categorical", prior = prior, pedigree = cleanPedi, data = as.data.frame(cleanData), nitt = 1e+05, burnin = 10000, thin = 30, pr=TRUE)
end = Sys.time()    # timing the function
end-start           # timing the function

save(model, file="model.rda")
```

```{r}
load(file="model.rda")

#diagnostics 
# autocorr.diag(model$Sol) # error on calculation: "Error: cannot allocate vector of size 15.6 Gb"
autocorr.diag(model$VCV)  # not great

plot(model$VCV)
#mean(model$VCV[, 1]/(model$VCV[, 1] + model$VCV[, 5] + model$VCV[, 5] + model$VCV[, 6] + pi^2/3))

# look at heritability of 'density dispersal'
# TODO: LOOK UP BLUP, breeding values (over time?)
### breeding value a is simulated via rbv w/ covar matrix (see iPad notes)
# DVs????????
# what is the distribution of density-dependent dispersal (if p_moving is binary)
# which one is a (breeding values) or are you supposed to use V_a for that
# possibilities for the ddd thing as response
### multitraits model
### calculate reaction norms before hand and add into dataset
```
```{r}
summary(model)
```





```{r}
# Test: binary data, no fixed or random effects
## Fit model
prior <- list(R = list(V = 1, fix = 1), G = list(G1 = list(V = 1, nu = 1000, alpha.mu = 0, alpha.V = 1)))
test_model <- MCMCglmm(p_moving ~ 1, random = ~animal, family = "ordinal", prior = prior, pedigree = test_pedigree, data = test_dataset, nitt = 1e+05, burnin = 10000, thin = 100)
## low nitt & burnin for testing purposes

## heritability of movement
 ## +1 in denom bc of probit link var
 ## NOTE: papers says dispersal has binom distribution w/ logit link? family="categorical"?
mean(test_model$VCV[, "animal"]/(test_model$VCV[, "animal"] + test_model$VCV[, "units"] + 1))
```

```{r}
# Test: binary data, some fixed effects (including sex)
## Fit model
prior <- list(R = list(V = 1, fix = 1), G = list(G1 = list(V = 1, nu = 1000, alpha.mu = 0, alpha.V = 1)))
test_model <- MCMCglmm(p_moving ~ sex + s_loc_density*s_land_density*s_time + s_loc_density*s_dkin, random = ~animal, family = "ordinal", prior = prior, pedigree = test_pedigree, data = test_dataset, nitt = 1e+05, burnin = 10000, thin = 100)
## s_loc_density*s_land_density*s_time + s_loc_density*s_dkin + (1|animal) + (1|reach) + (s_loc_density|sampling)
## what does (1|animal) do?
## if the non-included var is included, returns warning message: some fixed effects are not estimable and have been removed. Use singular.ok=TRUE to sample these effects, but use an informative prior!

## heritability of movement, with fixed effects in mind (much lower)
 ## +1 in denom bc of probit link var
mean(test_model$VCV[, "animal"]/(test_model$VCV[, "animal"] + test_model$VCV[, "units"] + 1))
```

```{r}
# Test: binary data, some fixed effects, sex split (Females)
## Fit model
prior <- list(R = list(V = 1, fix = 1), G = list(G1 = list(V = 1, nu = 1000, alpha.mu = 0, alpha.V = 1)))
test_model <- MCMCglmm(p_moving ~ s_loc_density*s_land_density*s_time + s_loc_density*s_dkin, random = ~animal, family = "categorical", prior = prior, pedigree = test_pedigree, data = test_dataset[test_dataset$sex=="F",], nitt = 1e+05, burnin = 10000, thin = 100)


## heritability of movement in F, mixed models 
 ## +1 in denom bc of probit link var
mean(test_model$VCV[, "animal"]/(test_model$VCV[, "animal"] + test_model$VCV[, "units"] + 1))
```

```{r}
# Test: multivariate model (semi-arbitary traits chosen, just testing viability)
## Fit model
prior<- list(R=list(V=diag(2)*(0.002/1.002),nu=1.002),
             G=list(G1=list(V=diag(2)*(0.002/1.002),nu=1.002)))
test_multi <- MCMCglmm(cbind(s_time,s_land_density) ~trait-1,random=~us(trait):animal,
rcov=~us(trait):units,family=c("gaussian","gaussian"), prior = prior, pedigree = test_pedigree, data = test_dataset, nitt = 1e+05, burnin = 10000, thin = 100)

## heritability of two traits
herit1<-test_multi$VCV[,'traits_time:traits_time.animal']/(test_multi$VCV[,'traits_time:traits_time.animal']+test_multi$VCV[,'traits_time:traits_time.units'])
mean(herit1)

herit2<-test_multi$VCV[,'traits_land_density:traits_land_density.animal']/
(test_multi$VCV[,'traits_land_density:traits_land_density.animal']+test_multi$VCV[,'traits_land_density:traits_land_density.units'])
mean(herit2)

## genetic correlation between the two traits (nonsense in this case)
corr.gen <- test_multi$VCV[,'traits_time:traits_land_density.animal']/sqrt(test_multi$VCV[,'traits_time:traits_time.animal']*test_multi$VCV[,'traits_land_density:traits_land_density.animal'])
mean(corr.gen)
```

```{r}
# Test: multivariate model (adult dispersal & density (questionable prior setup?))
## Fit model
prior<- list(R=list(V=diag(2)*(0.002/1.002),nu=1.002),
             G=list(G1=list(V=diag(2)*(0.002/1.002),nu=1.002)))
test_multi <- MCMCglmm(cbind(p_moving,s_land_density) ~trait-1,random=~us(trait):animal,
rcov=~us(trait):units,family=c("categorical","gaussian"), prior = prior, pedigree = test_pedigree, data = test_dataset, nitt = 1e+05, burnin = 10000, thin = 100)

## heritability of two traits
herit1<-test_multi$VCV[,'traitp_moving.1:traitp_moving.1.animal']/(test_multi$VCV[,'traitp_moving.1:traitp_moving.1.animal']+test_multi$VCV[,'traitp_moving.1:traitp_moving.1.units'])
mean(herit1)

herit2<-test_multi$VCV[,'traits_land_density:traits_land_density.animal']/
(test_multi$VCV[,'traits_land_density:traits_land_density.animal']+test_multi$VCV[,'traits_land_density:traits_land_density.units'])
mean(herit2)

## genetic correlation between the two traits (nonsense in this case)
corr.gen <- test_multi$VCV[,'traitp_moving.1:traits_land_density.animal']/sqrt(test_multi$VCV[,'traitp_moving.1:traitp_moving.1.animal']*test_multi$VCV[,'traits_land_density:traits_land_density.animal'])
mean(corr.gen)
```

```{r}
# Test: multivariate model (adult loc dispersal & density (questionable prior setup?)), with fixed effects INCLUDING sex
## Fit model
prior<- list(R=list(V=diag(2)*(0.002/1.002),nu=1.002),
             G=list(G1=list(V=diag(2)*(0.002/1.002),nu=1.002)))
test_multi <- MCMCglmm(cbind(p_moving,s_loc_density) ~trait + sex -1,random=~us(trait):animal,
rcov=~us(trait):units,family=c("categorical","gaussian"), prior = prior, pedigree = test_pedigree, data = test_dataset, nitt = 1e+05, burnin = 10000, thin = 100)

## heritability of two traits
herit1<-test_multi$VCV[,'traitp_moving.1:traitp_moving.1.animal']/(test_multi$VCV[,'traitp_moving.1:traitp_moving.1.animal']+test_multi$VCV[,'traitp_moving.1:traitp_moving.1.units'])
mean(herit1)

herit2<-test_multi$VCV[,'traits_loc_density:traits_loc_density.animal']/
(test_multi$VCV[,'traits_loc_density:traits_loc_density.animal']+test_multi$VCV[,'traits_loc_density:traits_loc_density.units'])
mean(herit2)

## genetic correlation between the two traits (nonsense in this case)
corr.gen <- test_multi$VCV[,'traitp_moving.1:traits_loc_density.animal']/sqrt(test_multi$VCV[,'traitp_moving.1:traitp_moving.1.animal']*test_multi$VCV[,'traits_loc_density:traits_loc_density.animal'])
mean(corr.gen)
```





```{r}
## testing whole thing (errors)
prior <- list(R = list(V = 1, fix = 1), G = list(G1 = list(V = 1, nu = 1000, alpha.mu = 0, alpha.V = 1)))
test_model <- MCMCglmm(p_moving ~ 1, random = ~animal, family = "ordinal", prior = prior, pedigree = test_pedigree, data = DDDdata, nitt = 1e+05, burnin = 10000, thin = 100)
```




### NOTES TO SELF (DELETE FROM FINAL)

- FOR THE LOVE OF GOD VECTORIZE
- An individual can be in pedigree and not in data, but anything in data MUST also be in the pedigree
- BLUP



```{r}
# NA in p_moving is fine 
# so are fish being in pedigree but not in dataset
#p_moving ~ Density + ratio male to female + kin + landscape 
# as random effects : (Density|animal) + (1|section)  # FACTOR THESE INTO h2 denom


# get male to female ratio for each reach at each time point
DDDdata <- DDDdata %>% 
  group_by(reach, time) %>%
  mutate(female = table(sex)["F"], male = table(sex)["M"], MtF = female/male)


cleanData <- DDDdata[complete.cases(DDDdata[, c("MtF", "s_loc_density", "s_dkin", "s_land_density")]),]

## testing whole thing (errors)
prior <- list(R = list(V = 1, nu = 0.002), G = list(G1 = list(V = 1,
nu = 0.002), G2 = list(V = 1, nu = 0.002)))

start = Sys.time()  # timing the function
model <- MCMCglmm(p_moving ~ MtF + s_loc_density + s_dkin + s_land_density, random = ~animal, family = "categorical", prior = prior, pedigree = cleanPedi, data = as.data.frame(cleanData), nitt = 100000, burnin = 1000, thin = 10)
end = Sys.time()    # timing the function
end-start           # timing the function

```

```{r}
# animals with parents start at row 736
# for (i in 736:7221) {
#   if (!is.na(cleanPedi$sire[i])) {
#     
#     in_animal = which(cleanPedi$animal == cleanPedi$sire[i])
#     
#     if (in_animal >= i) {
#       print(paste("Animal", cleanPedi$sire[i], "appears in sire row", i, "before animal row", in_animal))
#       
#     }
#   }
# }
# 
# for (i in 736:7221) {
#   if (!is.na(cleanPedi$dam[i])) {
#     
#     in_animal = which(cleanPedi$animal == cleanPedi$dam[i])
#     
#     if (in_animal >= i) {
#       print(paste("Animal", cleanPedi$dam[i], "appears in dam row", i, "before animal row", in_animal))
#       
#     }
#   }
# }
```






```{r }
# #Dispersal measured as boolean event, assumed to follow binomial w/ logit link
# 
# #local density, landscape density (individuals/m), time (months) + 2 & 3 way interactions as fixed explanatory var
# 
# model.pmov1 <- glmer(p_moving~s_loc_density*s_land_density*s_time +
#                     + s_loc_density*s_dkin +
#                     + (1|FishID) + (1|reach) +li (s_loc_density|sampling),
#                     + (1|animal) + (1|reach) +li (s_loc_density|sampling),
#                     data=subset(fata, habitat=='P'),
#                     family='binomial',
#                     control=glmerControl(optCtrl=list(maxfun=1e5), optimizer='bobyqa'),
#                     na.action=na.omit)

```

```{r}

# #1  pheno, only fixed is pop mu
# prior <- list(R = list(V=1, nu=0.002), G = list(G1 = list(V=1, nu=0.002)))
# model <- MCMCglmm(phen ~ 1, random = ~animal, family = "gaussian",
# prior = prior, pedigree = pedigree, data = data, nitt = 100000,
# burnin = 10000, thin = 10)
# herit <- model$VCV[, "animal"]/(model$VCV[, "animal"] + model$VCV[, "units"])
# 
# #2 multiple response traits
# prior<-
# list(R=list(V=diag(2)/2,nu=2),
# G=list(G1=list(V=diag(2)/2,nu=2)))
# 
# modelmulti<-MCMCglmm(cbind(phen1,phen2)~trait-1,random=~us(trait):animal,
# rcov=~us(trait):units,family=c("gaussian","gaussian"),prior=prior,
# pedigree=pedigreemulti,data=datamulti,nitt=100000,burnin=10000,thin=10)
# herit1<-modelmulti$VCV[,'phen1:phen1.animal']/
# (modelmulti$VCV[,'phen1:phen1.animal']+modelmulti$VCV[,'phen1:phen1.units'])
# herit2<-modelmulti$VCV[,'phen2:phen2.animal']/
# (modelmulti$VCV[,'phen2:phen2.animal']+modelmulti$VCV[,'phen2:phen2.units'])
# mean(herit1)
#
#
# ##correlation between 2 traits
# 
# corr.gen<-modelmulti$VCV[,'phen1:phen2.animal']/
# sqrt(modelmulti$VCV[,'phen1:phen1.animal']*modelmulti$VCV[,'phen2:phen2.animal']); mean(corr.gen)
```
