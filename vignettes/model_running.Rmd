---
title: ''
author: "Weixin Du"
output: 
  html_document:
    toc: true
    toc_float: true
    toc_depth: 4
    theme: flatly
    code_folding: hide
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


## Setup
```{r, message=FALSE, warning=FALSE}
# loading required packages
# for installing on different computer: just use binary not source
library(tidyverse)
library(MCMCglmm)
library(pedigree)
library(MasterBayes)
# library(sticky)
# library(lme4)

load("../data_raw/DDDdata.rda")
pedigree <- read.csv2("../data_raw/Pedigree.csv", row.names = NULL)
# rename "FishID" column to "animal"
colnames(DDDdata)[2] <- "animal"
colnames(pedigree)[2] <- "animal"
colnames(pedigree)[6:7] <- c("sire", "dam")

# fata <- subset(data, sex == 'F')
# mata <- subset(data, sex == 'M')
```

### Clean the data
```{r}

## remove all fish in data not in pedigree (including sire & dam)
# checks if a fish in pedigree is in DDDdata, if not then it goes on the remove list
present <- unique(DDDdata$animal) %in% unique(pedigree$animal)
remove <- unique(DDDdata$animal)[which(present == FALSE)]
removable <- which(DDDdata$animal %in% remove == TRUE)
DDDdata <- as.data.frame(DDDdata[-removable, ])


# in the order required for MCMCglmm
cleanPedi <- pedigree[, c("animal", "sire", "dam")]
# convert blanks to NA, add moms and dads to the animal column
cleanPedi[cleanPedi == ""] <- NA
cleanPedi <- add.Inds(cleanPedi)


## get rid of all the duplicates
# get first instance of each fish, then keeps only those rows (assume rest are blank duplicates)
cleanPedi[cleanPedi == ""] <- NA
cleanPedi <- cleanPedi[match(unique(cleanPedi$animal), cleanPedi$animal), ]


# filter and reorder pedigree
cleanPedi <- orderPed(cleanPedi)


# present <- unique(cleanPedi$animal) %in% unique(DDDdata$animal)
# remove <- unique(cleanPedi$animal)[which(present == FALSE)]
# removable <- which(cleanPedi$animal %in% remove == TRUE)
# cleanPedi <- cleanPedi[-removable, ]
```

```{r}
cleanData <- DDDdata[-which(is.na(DDDdata$s_loc_density)),]

start = Sys.time()    # timing the function
prior <- list(R = list(V = diag(1), fix = 1), G = list(G1 = list(V = diag(2), nu = 1000, alpha.mu = c(0, 0), alpha.V = diag(2)), G2 = list(V = 1, nu = 1000, alpha.mu = 0, alpha.V = 1)))

model_stable <- MCMCglmm(p_moving ~ s_loc_density + s_time, random = ~ idh(s_loc_density+1):animal + reach, family = "threshold", prior = prior, pedigree = cleanPedi, data = as.data.frame(cleanData), nitt = 1e+05, burnin = 10000, thin = 10, pr=TRUE)
end = Sys.time()    # timing the function
end-start           # timing the function

save(model_stable, file="model_stable.rda")
```

```{r}
load(file="model_stable.rda")
autocorr.diag(model_stable$VCV)  
plot(model_stable$VCV)
summary(model_stable)
```









